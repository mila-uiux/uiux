import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'UIUX-SAAS';
  roles = [
    {
      name: 'View',
      description: 'Reference site about Lorem Ipsum, giving information on its origins.'
    },
    {
      name: 'Create',
      description: 'Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts.'
    },
    {
      name: 'Edit',
      description: 'Provides an elegant and quick way to create default text or generate Lorem Ipsum.'
    },
    {
      name: 'Delete',
      description: 'Reference site about Lorem Ipsum, giving information on its origins.'
    },
  ];
}
